require 'rails_helper'

describe "Deleting Posts" do
  let!(:post) {Post.create!(title: "Editing titles", description: "Editing the description", team: "Yankees")} 
  it "deletes the post" do
    visit root_path
    within dom_id_for(post) do
      click_link "Delete"
    end
    
    expect(Post.count).to eq(0)
    expect(page).to_not have_content("Editing Articles")
    expect(post.description).to_not eq("editing the description")
  end
end