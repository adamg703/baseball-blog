require 'rails_helper'

describe 'Creating a new post' do
  it "saves with valid title, description, and team" do
    visit "posts/new"
    
    fill_in "Title", with: "Baseball post"
    fill_in "Description", with: "Baseball Description"
    select "Yankees", :from => "post_team"
    click_button "Create Post"
    
    expect(page).to have_content("Baseball post")
    
    expect(Post.count).to eq(1)
  end
  
  it "doesn't save with no title" do
    visit "posts/new"
    
    fill_in "Title", with: ""
    fill_in "Description", with: "Baseball"
    select "Yankees", :from => "post_team"
    click_button "Create Post"
    
    expect(page).to have_content("Title can't be blank")
    expect(Post.count).to eq(0)
  end
  
  it "doesn't save with no description" do
    visit "posts/new"
    
    fill_in "Title", with: "Baseball"
    fill_in "Description", with: ""
    select "Yankees", :from => "post_team"
    click_button "Create Post"
    
    expect(page).to have_content("Description can't be blank")
    expect(Post.count).to eq(0)
  end
end