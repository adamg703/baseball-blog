require 'rails_helper'

describe "Editing Posts" do
  let!(:post) {Post.create!(title: "Editing titles", description: "Editing the description", team: "Blue Jays")} 
  it "updates post when everything is filled out correctly" do
    visit root_path
    within dom_id_for(post) do
      click_link "Edit"
    end
    expect(page).to have_content("Editing Post")
    
    fill_in "Title", with: "Baseball post"
    fill_in "Description", with: "Baseball Description"
    select "Yankees", :from => "post_team"
    click_button "Update Post"
    
    expect(page).to have_content("Baseball post")
    click_link "Back"
    
    expect(Post.count).to eq(1)
    expect(page).to have_content("Baseball post")
  end
  
  it "doesn't update post when there is no title" do
    visit root_path
    within dom_id_for(post) do
      click_link "Edit"
    end
    
    expect(page).to have_content("Editing Post")
    
    fill_in "Title", with: ""
    fill_in "Description", with: "Baseball"
    select "Yankees", :from => "post_team"
    click_button "Update Post"
    
    expect(page).to have_content("Title can't be blank")
    expect(Post.count).to eq(1)
    expect(post.title).to eq("Editing titles")
  end
  
  it "doesn't update post when there is no description" do
    visit root_path
    within dom_id_for(post) do
      click_link "Edit"
    end
    
    expect(page).to have_content("Editing Post")
    
    fill_in "Title", with: "Baseball"
    fill_in "Description", with: ""
    select "Yankees", :from => "post_team"
    click_button "Update Post"
    
    expect(page).to have_content("Description can't be blank")
    expect(Post.count).to eq(1)
    expect(post.title).to eq("Editing titles")
  end
end
