require 'rails_helper'

describe "Showing posts" do
  let!(:post) {Post.create!(title: "Showing titles", description: "Showing the description", team: "Yankees")} 
  it "shows the post when you click on the show link" do
    visit root_path
    within dom_id_for(post) do
      click_link "Show"
    end
    
    expect(page).to have_content("This post is about the Yankees")
  end
end