require 'rails_helper'

describe "Editing Comments" do
  let!(:post) {Post.create!(title: "Baseball Post", description: "Description", team: "Yankees")}
 
  let!(:post_comment) {post.post_comments.create!(comment: "Comment for the post")}
  
  it "Updates the comment when everything is filled in" do
    visit root_path
    within dom_id_for(post) do
      click_link "Comments"
    end
    
    expect(page).to have_content(post_comment.comment)
    
    within dom_id_for(post_comment) do
      click_link "Edit"
    end
    
    fill_in "Comment", with: "New edited comment"
    click_button "Submit"
    
    expect(page).to have_content("New edited comment")
    expect(page).to_not have_content("Comment for the post")
    expect(page).to have_content("Comment successfully updated")
  end
  
  it "doesn't update the comment when comment isn't filled in" do
    visit root_path
    within dom_id_for(post) do
      click_link "Comments"
    end
    
    expect(page).to have_content(post_comment.comment)
    
    within dom_id_for(post_comment) do
      click_link "Edit"
    end
    
    fill_in "Comment", with: ""
    click_button "Submit"
    
    expect(page).to have_content("Comment can't be blank")
    expect(post_comment.comment).to eq("Comment for the post")
  end
end
