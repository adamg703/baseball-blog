require 'rails_helper'

describe "Showing Post comments" do
  let!(:post) {Post.create!(title: "Baseball Post", description: "Description", team: "Yankees")}
 
  let!(:post_comment) {post.post_comments.create!(comment: "Comment for the post")}
  
  it "Shows the comments when you click the link" do
    visit root_path
    within dom_id_for(post) do
      click_link "Comments"
    end
    
    expect(page).to have_content("Comment for the post")
  end
end
    