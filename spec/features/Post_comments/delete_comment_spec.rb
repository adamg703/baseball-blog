require 'rails_helper'

describe "Deleting Comments" do
  let!(:post) {Post.create!(title: "Baseball Post", description: "Description", team: "Yankees")}
 
  let!(:post_comment) {post.post_comments.create!(comment: "Comment for the post")}
  
  it "Deletes the comment when delete is pressed" do
    visit root_path
    within dom_id_for(post) do
      click_link "Comments"
    end
    
    expect(page).to have_content(post_comment.comment)
    
    within dom_id_for(post_comment) do
      click_link "Delete"
    end
    
    expect(page).to_not have_content("Comment for this post")
    expect(page).to have_content("The comment was successfully deleted")
  end
end