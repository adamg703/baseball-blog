require 'rails_helper'

describe "Adding comments" do
  let!(:post) {Post.create!(title: "Baseball Post", description: "Description", team: "Yankees")}

  it "adds the new comment when the comment field is filled out" do
    visit root_path
    click_link "Comments"
    
    click_link "Add Comment"
    
    expect(page).to have_content("New comment")
  
    fill_in "Comment", with: "New comment"
    click_button "Submit"
  
    expect(page).to have_content("Comment added successfully!")
    expect(PostComment.count).to eq(1)
  end
  
  it "doesn't save when the comment isn't filled out" do
    visit root_path
    click_link "Comments"
    
    click_link "Add Comment"
    
    expect(page).to have_content("New comment")
  
    fill_in "Comment", with: ""
    click_button "Submit"
  
    expect(page).to have_content("Comment can't be blank")
    expect(PostComment.count).to eq(0)
  end
end
