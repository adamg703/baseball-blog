module PostHelpers
  def set_post_params(options={})
    options[:title] ||= "Post title"
    options[:description] ||= "Post description"
    options[:team] ||= "Yankees"

    fill_in "Title", with: options[:title]
    fill_in "Description", with: options[:description]
    select options[:team], :from => "post_team"
  end
end