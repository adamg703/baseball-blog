class PostCommentsController < ApplicationController
  
  before_action :find_todo_list
  
  def index
    @post_comment = @post.post_comments.all
  end
  
  def show
    @post_comment = @post.post_comments.find(params[:id])
  end

  def new
    @post_comment = @post.post_comments.new
  end
  
  def edit
    @post_comment = @post.post_comments.find(params[:id])
  end
  
  def create
    @post_comment = @post.post_comments.new(post_comment_params)
    if 
      @post_comment.save
      redirect_to post_post_comments_path
      flash[:success] = "Comment added successfully!"
    else
      render 'new'
    end
  end

  def update
    @post_comment = @post.post_comments.find(params[:id])
    if
      @post_comment.update(post_comment_params)
      redirect_to post_post_comment_path
      flash[:success] = "Comment successfully updated"
    else
      render 'edit'
    end
  end

  def destroy
    @post_comment = @post.post_comments.find(params[:id])
    @post_comment.destroy
    
    redirect_to post_post_comments_path
    flash[:success] = "The comment was successfully deleted"
  end
end

private

  def find_todo_list
    @post = Post.find(params[:post_id])
  end
 
  def post_comment_params
    params[:post_comment].permit(:comment)
  end