class PostsController < ApplicationController
  before_action :set_team_options
   
  def index
    @post = Post.all
  end
  
  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end
  
  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to @post
      flash[:success] = "The post was successfully created!"
    else 
      render 'new'
    end
  end
  
  def edit
    @post = Post.find(params[:id])
  end
  
  def update
    @post = Post.find(params[:id])
    if @post.update(post_params)
      redirect_to @post
      flash[:success] = "The post has been updated successfully!"
    else
      render 'edit'
    end
  end   
  
  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    
    flash[:success] = "Post successfully deleted"
    redirect_to root_path
  end
end

  
private
def post_params
  params.require(:post).permit(:title, :description, :team)
end
  
  
