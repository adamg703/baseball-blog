class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # For the dropdown menu in Blog Posts
  def set_team_options
     @mlb_teams =
      ["Yankees", "Yankees"],
      ["Orioles", "Orioles"],
      ["Red Sox", "Red Sox"],
      ["Blue Jays", "Blue Jays"],
      ["Rays", "Rays"],
      ["White Sox", "White Sox"],
      ["Indians", "Indians"],
      ["Tigers", "Tigers"],
      ["Royals", "Royals"],
      ["Twins", "Twins"],
      ["Astros", "Astros"],
      ["Angels", "Angels"],
      ["Atheltics", "Athletics"],
      ["Mariners", "Mariners"],
      ["Rangers", "Rangers"],
      ["Giants", "Giants"],
      ["Padres", "Padres"],
      ["Dodgers", "Dodgers"],
      ["Rockies", "Rockies"],
      ["Diamondbacks", "Diamondbacks"],
      ["Cardinals", "Cardinals"],
      ["Pirates", "Pirates"],
      ["Brewers", "Brewers"],
      ["Reds", "Reds"],
      ["Cubs", "Cubs"],
      ["Braves", "Braves"],
      ["Marlins", "Marlins"],
      ["Mets", "Mets"],
      ["Phillies", "Phillies"],
      ["Nationals", "Nationals"]
  end
end