class PostComment < ActiveRecord::Base
  validates_presence_of :comment
  belongs_to :post
end
