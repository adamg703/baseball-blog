class Post < ActiveRecord::Base
  validates_presence_of :title
  validates_presence_of :description
  validates_presence_of :team
  has_many :post_comments
end
  