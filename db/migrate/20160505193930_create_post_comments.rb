class CreatePostComments < ActiveRecord::Migration
  def change
    create_table :post_comments do |t|
      t.belongs_to :post, index: true
      t.text :comment
      
      t.timestamps null: false
    end
    add_foreign_key :post_comments, :posts
  end
end
